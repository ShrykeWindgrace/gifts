# Gifts

## Pens and inks

I used to be interested in fountain pens and I wanted to try so-called `flex nibs`.
Back in the day I had my eyes on **Pilot Falcon** (120-200 euros depending on the body material, plastic/metal).

Now I almost never write on paper, so that hobby is no more.

## Games

I do not play video games. At all - I simply do not have time to concentrate on game for an extended period of time.

Sometimes I play chess recreationally.

## Alcohol

Rich sweet red wines (like Maury and Banyuls) or alsacian wines (e.g. Gewurtztraminer) are a safe bet.
I am not that keen on rose wines.
Young cognacs are probably fine, too.

## Tea

I drink black teas like Assam. Just the black tea itself, no other spices, herbs, berries, or perfumes (especially bergamot!).
The only exception being blackcurrant leaves.

